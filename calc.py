def sumar(numero1, numero2):
    return numero1 + numero2


def restar(numero1, numero2):
   return numero1 - numero2


print("1 + 2 = " + str(sumar(1, 2)))
print("3 + 4 = " + str(sumar(3, 4)))
print("6 - 5 = " + str(restar(6, 5)))
print("8 - 7 = " + str(restar(8, 7)))
